﻿# Đại học khoa học tự nhiên
# -------------------------------------------------
###Họ tên: NGUYỄN HỒ QUỐC THỊNH
###MSSV 1512544
# -------------------------------------------------
#Chức năng đã hoàn thành:
1. Đưa hook vào dll
2. Ấn phím Q để install/uninstall hook
3. Khi kích hoạt hook thì chuột trái bị vô hiệu hóa
#Luồng sự kiện chính:
* Người dùng khởi động chương trình
* Click vào button Press Me => xuất hiện hộp thoại
* Ấn phím Q => install hook
* Click vào button Press Me không được do chuột trái bị vô hiệu hóa
* Ấn phím Q => uninstall hook
* Ấn vào button Press Me => xuất hiện hộp thoại, do hook bị gở bỏ 
#Luồng sự kiện phụ:
*
#Link bitbucket: https://NguyenHoQuocThinh@bitbucket.org/NguyenHoQuocThinh/hook
#Link video demo hướng dẫn: 
